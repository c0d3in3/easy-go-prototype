package com.tedex.btu.easygoprototype

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_results.*
import kotlinx.android.synthetic.main.activity_second.*

class ResultsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)
        init()
    }
    private fun init(){
        var id3:String = intent.extras.getString("id3")
        var id4:String = intent.extras.getString("id4")

        if(id3 == "radioButton_6"){
            hotelImage_1.setBackgroundResource(R.drawable.stamba_logo)
            hotelImage_2.setBackgroundResource(R.drawable.biltmore_logo)
            hotelImage_3.setBackgroundResource(R.drawable.iveria_logo)
            hotelImage_4.setBackgroundResource(R.drawable.ambassadori_logo)
        }
        if(id3 == "radioButton_7"){
            hotelImage_1.setBackgroundResource(R.drawable.laerton_logo)
            hotelImage_2.setBackgroundResource(R.drawable.astoria_logo)
            hotelImage_3.setBackgroundResource(R.drawable.grove_logo)
            hotelImage_4.setBackgroundResource(R.drawable.rustaveli_logo)
        }
        if(id3 == "radioButton_8"){
            hotelImage_1.setBackgroundResource(R.drawable.beli_logo)
            hotelImage_2.setBackgroundResource(R.drawable.hotel_27_logo)
        }
        if(id3 == "radioButton_9"){
            hotelImage_1.setBackgroundResource(R.drawable.m42_logo)
            hotelImage_2.setBackgroundResource(R.drawable.fox_logo)
            hotelImage_3.setBackgroundResource(R.drawable.namaste_logo)
            hotelImage_4.setBackgroundResource(R.drawable.gallery_logo)
        }
        if(id4 == "radioButton_11"){
            clubImage_1.setBackgroundResource(R.drawable.gino_logo)
            clubImage_2.setBackgroundResource(R.drawable.neptune_logo)
            clubImage_3.setBackgroundResource(R.drawable.sakurami_logo)
            clubImage_4.setBackgroundResource(R.drawable.sam_raan_logo)
        }
        if(id4 == "radioButton_12"){
            clubImage_1.setBackgroundResource(R.drawable.basiano_logo)
            clubImage_2.setBackgroundResource(R.drawable.night_office_logo)
            clubImage_3.setBackgroundResource(R.drawable.khidi_logo)
            clubImage_4.setBackgroundResource(R.drawable.mtkvarze_logo)
        }
    }
}
