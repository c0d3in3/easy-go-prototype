package com.tedex.btu.easygoprototype

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }
    private fun init(){
        signUpButton.setOnClickListener {
            if(firstNameEditText.text.toString().isEmpty() || lastNameEditText.text.toString().isEmpty() || emailEditText.text.toString().isEmpty() || passwordEditText.text.toString().isEmpty()
            || passwordEditText.text.toString().isEmpty() || repeatPasswordEditText.text.toString().isEmpty() || numberEditText.text.toString().isEmpty() || !checkBox.isChecked) {
                Toast.makeText(this@RegisterActivity, "Please fill all fields!", Toast.LENGTH_LONG).show()
            }
            else{
                Toast.makeText(this@RegisterActivity, "You have successfully signed up!", Toast.LENGTH_LONG).show()
                val intent = Intent(this, SecondActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
