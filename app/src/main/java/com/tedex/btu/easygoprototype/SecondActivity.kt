package com.tedex.btu.easygoprototype

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init() {
        continueButton.setOnClickListener {
            val id1: Int = radioGroup1.checkedRadioButtonId
            val id2: Int = radioGroup2.checkedRadioButtonId
            val id3: Int = radioGroup3.checkedRadioButtonId
            val id4: Int = radioGroup4.checkedRadioButtonId
            if (id1 == -1 || id2 == -1 || id3 == -1 || id4 == -1) {
                Toast.makeText(this@SecondActivity, "Please fill all fields", Toast.LENGTH_SHORT).show()
            } else {
                val intent = Intent(this, ResultsActivity::class.java)
                if(radioButton_6.isChecked) intent.putExtra("id3", "radioButton_6")
                if(radioButton_7.isChecked) intent.putExtra("id3", "radioButton_7")
                if(radioButton_8.isChecked) intent.putExtra("id3", "radioButton_8")
                if(radioButton_9.isChecked) intent.putExtra("id3", "radioButton_9")
                if(radioButton_10.isChecked) intent.putExtra("id3", "radioButton_10")
                if(radioButton_11.isChecked) intent.putExtra("id4", "radioButton_11")
                if(radioButton_12.isChecked) intent.putExtra("id4", "radioButton_12")
                startActivity(intent)
            }
        }
    }
}
