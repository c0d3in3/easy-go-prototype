package com.tedex.btu.easygoprototype

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {
        logInButton.setOnClickListener {
            if(passwordEditText.text.toString().isEmpty()){
                Toast.makeText(this@MainActivity, "E-Mail field is empty!", Toast.LENGTH_LONG).show()
            }
            if(emailEditText.text.toString().isEmpty()){
                Toast.makeText(this@MainActivity, "Password field is empty!", Toast.LENGTH_LONG).show()
            }
            if(!emailEditText.text.toString().isEmpty() && !passwordEditText.text.toString().isEmpty()){
                val intent = Intent(this, SecondActivity::class.java)
                startActivity(intent)
            }
        }
        signUpButton.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }
}
